<script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class="altmetric-embed" data-badge-type="donut" data-altmetric-id="67130154" />

# It’s about time: Analysing simplifying assumptions for modelling multi-step pathways in systems biology

This repository contains the source code for the above paper.


The figures for the paper were all generated using this code. In fact, a continuous integration setup ensures that they are automatically created every new commit to this repository and they are accessible [here.](https://gitlab.com/slcu/teamHJ/publications/korsbo_et_al_2020/-/jobs/artifacts/master/browse/results/?job=test)

## Installation:
This was developed for Julia v1.4 but should also work with newer Julia versions up until v2.0. 
The important bits of the code are in source files under the `src` directory. 
However the creation of figures and the actual analyses where done in scripts under `scripts`. 
There are also Jupyter notebooks which you may either use interactively using Jupyter or just view on the GitLab page. 
The [tutorial.ipynb](https://gitlab.com/slcu/teamHJ/publications/korsbo_et_al_2020/blob/master/notebooks/tutorial.ipynb) notebook could be a good idea to look through, but everything we did is reproducible without installing Jupyter. 

- Install [Julia](https://julialang.org/downloads/).
- Install [Jupyter](https://jupyter.org/install) (optional).

- In your terminal, navigate to some suitable directory and clone this package in to a directory called `LinearNodes.jl`:
```bash
git clone https://gitlab.com/slcu/teamHJ/publications/korsbo_et_al_2020.git LinearNodes.jl
```

- Enter the newly created `LinearNodes.jl` directory and open a julia REPL session here:
```bash
cd LinearNodes.jl
julia --project
```

- Inside your REPL, instantiate the dependencies by:
```julia
using Pkg
Pkg.activate(".") # "." for current directory
Pkg.instantiate()
using LinearNodes

exit() # when you want to exit the REPL.
```

This instantiation reads a `Manifest.toml` file wherein we have specified the exact version and git commit of every dependency we have, direct or indirect. This procedure should, therefore, ensure that your setup is now identical to the one we used.

## Reproducing the results

There are two stages to the reproduction of our results: optimisation and analysis.

### Optimisation

At the core of this project lies the generation of synthetic data, coupled with the optimisation of different models towards reproducing that data.
Each batch of model optimisations only takes a few minutes but since we repeated this 30,000 times, this takes quite a long time. 
All of our results are saved as files and they are included in this repository. 
So, while we wish to provide the option of reproducing every step, you do not have to repeat this if you only want to move on to the analysis.

Optimisation is done using the `opt` or function found in `src/optimise.jl`. 
We ran optimisation for different conditions and different models. 
In the `scripts` folder
- `parallel_opt` - the linear pathway model optimisations with randomly distributed targets.
- `opt_homogeneous` - the linear pathway model optimisations where all targets are generated with homogeneous rate parameters.
-  `opt_inhomogeniety` - the linear pathway model optimisations where targets are generated with different ranges of logarithmically separated rate-parameter values.
- `parallel_opt_cascade` - the single-target cascade model optimisations.
- `parallel_opt_cascade_multi` - the three-target cascade model optimisations.

The `parallel_opt` script does much of the heavy lifting and it is written to facilitate the distribution of tasks across multiple cpu threads. 
It can be run (with for example 8 threads) by 
```bash
julia -p 8 scripts/parallel_opt.jl
```
This would take days if not weeks. We ran this on a cluster with 150 cpu cores over a weekend. If you just want to test it, you could edit the script and change the amount of repeats, `n`, to something small. Same goes for the other scripts prepended with "parallel".

The two remaining scripts are simpler since they are less computationally demanding. 
To run them, simply execute 
```bash
julia scripts/opt_homogeneous.jl
julia scripts/opt_inhomogeniety.jl
```

### Analysis and plotting

The base code for this project (located under `src/`) is used in two different ways: Either in scripts (located under `scripts/`) or in Jupyter notebooks (located under `notebooks/`). The figures in the paper can all be reproduced using the scripts which does not require you to get Jupyter up and running. 

There are two ways of interacting with the scripts. You can either run them straight up by 
```bash
julia scipts/<script name>.jl
```

or you can use a REPL or IDE to execute them chunk-by-chunk so that you can interact more with the process. 


If you don't mind getting a Jupyter notebook up and running (it should be pretty easy) then I would recommend that you start with looking through
`notebooks/tutorial.ipynb` first. This shows you how to interact with the code and makes everything else easier to understand. It also shows you how many of the actual analyses were made without obfuscating matters by simultaneously trying to fine-tune plotting, etc., for publication. 


To run the tutorial, first open a jupyter session in the `notebooks` folder:
```bash
cd path/to/your/LinearNodes.jl
cd notebooks
jupyter notebook
```

The plots in the publication where created using the PGFPlotsX.jl backend for [Plots.jl](http://docs.juliaplots.org/latest/).
This requires `LaTeX` or `LuaTeX` to be installed. 
If you do not want to fiddle with this, then replacing any `pgfplotsx()` command with `gr()` switches to a pre-installed plotting backend. The only issue you may encounter then is with LaTeX formatted strings in the plots. 


To just run all of the analyses and reproduce all the plots, run
```bash
julia scripts/run_all_plots.jl
```

If you just want to reproduce (or inspect) specific figures we'll list the relevant scripts here (all are in the `scripts` folder).

- Fig 1 - `plot_display.jl`
- Fig 2 - `plot_param_analysis_fixed_step.jl`
- Fig 3 - `plot_inhomogeniety_analysis.jl`
- Fig 4 - `plot_sharpness.jl`
- Fig 5 - `plot_display.jl`
- Fig 6 - `plot_multi_input.jl`
- Fig 7 - `plot_biology_examples.jl`
- Fig 8 - `plot_dde.jl`
- Fig 9 - `plot_gamma_comparison.jl`
- Fig 10 - `plot_cost_vs_nonlinearity.jl`
- Fig 11 - `plot_cascade_fit.jl`
- Fig 12 - `plot_cost_demo.jl`
- SI1 Fig - `plot_display.jl`
- SI2 Fig - `plot_display.jl`
- SI3 Fig - `plot_display.jl`
- SI4 Fig - `plot_display.jl`
- SI5 Fig - `plot_display.jl`
- SI6 Fig - `plot_truncated_cost.jl`
