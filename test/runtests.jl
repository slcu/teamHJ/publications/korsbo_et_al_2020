using Pkg
Pkg.activate("../")

using Test
using LinearNodes

@testset "Statistics-based parameter estimation" begin include("analytic_parameter_estimation.jl") end
@testset "Model simulations actually run" begin include("simulate.jl") end
