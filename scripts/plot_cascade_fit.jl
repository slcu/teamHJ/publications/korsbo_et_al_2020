using FileIO
using Statistics
using Plots
pgfplotsx()
using LinearNodes
using ProgressMeter
using DifferentialEquations
Plots.default(grid=false)

root = joinpath(splitpath(pathof(LinearNodes))[1:end-2]...)

# save("parameters/ResultCascade.jld2", "pc", pcc)
pcc = load("parameters/ResultMultiCascadeMultiTargetRedefined2.jld2", "pc")

pcs = load("parameters/ResultMultiCascadeSingleRedefined.jld2", "pc")
sample_idx = 42

#==============================================================================#
#=============================  Calculate stuff  ==============================# 
#==============================================================================#


filename = joinpath(root, "parameters", "cost_matrix_cascade_prediction.jld2")

if isfile(filename)
    cost_dict = load(filename)
    cost_matrix = cost_dict["multi_fr"]
    cost_matrix_cascade = cost_dict["multi_cascade"]
    cost_matrix_single = cost_dict["single_fr"]
    cost_matrix_single_2 = cost_dict["single_cascade"]
    scales = cost_dict["scales"]
else
    ## This takes quite a bit of time if one cranks up the length of the scales array.
    ## Expect it to take at least 5 minutes.
    scales = 10. .^ range(-5, stop=5, length=11) 

    cost_matrix = @showprogress [evaluate_cost(
            p,
            :fixed_rate_cascade,
            PulseInput(γ,1.),
            alg=Rosenbrock23(),
            tstop=p.tstops[end],
        ) for p in pcc, γ in scales]

    cost_matrix_cascade = @showprogress [evaluate_cost(
        p,
        :cascade_2,
        PulseInput(γ, 1.),
        alg=Rosenbrock23(),
        tstop=p.tstops[end],
    ) for p in pcc, γ in scales]

    cost_matrix_single = @showprogress [evaluate_cost(
        p,
        :fixed_rate_cascade,
        PulseInput(γ,1.),
        alg=Rosenbrock23(),
        tstop=p.tstops[end],
    ) for p in pcs, γ in scales]

    cost_matrix_single_2 = @showprogress [evaluate_cost(
        p,
        :cascade_2,
        PulseInput(γ,1.),
        alg=Rosenbrock23(),
        tstop=p.tstops[end],
    ) for p in pcs, γ in scales]

    save(filename, "multi_fr", cost_matrix, "multi_cascade", cost_matrix_cascade, "single_fr", cost_matrix_single, "single_cascade", cost_matrix_single_2, "scales", scales)
end



mask_cm = [all(0 .< x .< 1.) && !any(isnan.(x)) for x in eachrow(cost_matrix)]
mask_cm2 = [all(0 .< x .< 1.) && !any(isnan.(x)) for x in eachrow(cost_matrix_cascade)]
mask_cms = [all(0 .< x .< 1.) && !any(isnan.(x)) for x in eachrow(cost_matrix_single)]
mask_cms2 = [all(0 .< x .< 1.) && !any(isnan.(x)) for x in eachrow(cost_matrix_single_2)]

println("$(round(Int, mean(mask_cm)*100))% of fixed-rate lines retained after filtering multi-opt.")
println("$(round(Int, mean(mask_cm2)*100))% of fixed-rate lines retained after filtering multi-opt.")
println("$(round(Int, mean(mask_cms)*100))% of fixed-rate lines retained after filtering single-opt.")
println("$(round(Int, mean(mask_cms2)*100))% of two-step lines retained after filtering single-opt.")

1 - (sum(mask_cm) + sum(mask_cms))/(length(mask_cm) + length(mask_cms))
1 - (sum(mask_cm2) + sum(mask_cms2))/(length(mask_cm2) + length(mask_cms2))

masks = [mask_cm, mask_cm2, mask_cms, mask_cms2]
identifiers = [
    "triple target, fixed-rate",
    "triple target, two-step",
    "single target, fixed-rate",
    "single target, two-step",
]
for i in 1:4
    println("$(length(masks[i]) - sum(masks[i]))/$(length(masks[i]))    for $(identifiers[i])")
end

#====  Find a data set with results that represent the "average" dynamics  ====# 
square_cost = mean(
    (cost_matrix[mask_cm .* mask_cm2, [3, 5, 8]] .- 
     mean(cost_matrix[mask_cm .* mask_cm2, [3, 5, 8]], dims=1)) .^2 .+ 
    (cost_matrix_cascade[mask_cm .* mask_cm2, [3, 5, 8]] .- 
     mean(cost_matrix_cascade[mask_cm .* mask_cm2, [3, 5, 8]], dims=1)) .^2,
    dims=2,
)

square_cost_single = mean(
    (cost_matrix_single[mask_cms .* mask_cms2, :] .- 
     mean(cost_matrix_single[mask_cms .* mask_cms2, :], dims=1)) .^2 .+ 
    (cost_matrix_single_2[mask_cms .* mask_cms2, :] .- 
     mean(cost_matrix_single_2[mask_cms .* mask_cms2, :], dims=1)) .^2,
    dims=2,
)
square_cost_single


#==========================  Generate a noisy input  ==========================# 

target = simulate(CascadeModel(), NoiseInput(), [1., 1, 1]; tstop=500.)
noise = RepeatedNoiseInput(target)

#==============================================================================#
#===============================  Plot figure  ================================# 
#==============================================================================#

sample_idx_single = eachindex(pcs)[mask_cms .* mask_cms2][sortperm(vec(square_cost_single))[4]]
sample_idx = eachindex(pcc)[mask_cm .* mask_cm2][sortperm(vec(square_cost))[8]]
# sample_idx = 103
# sample_idx
# sample_idx_single = 3
scale_noise = 2e-6
p = pcc[sample_idx]
p_single = pcs[sample_idx_single]
model_key = :fixed_rate_cascade
plot(layout=(2,4), size = (800, 350), dpi=270, grid=false);
plot!(title = permutedims(["$fig" for fig in 'A':'Z']), title_location=(0.5, 0.95));
#============================  Single-opt figures  ============================# 
plot!(subplot=1, p_single; lw=[3 2 2], plotmodels=[:data, :cascade_2, :fixed_rate_cascade], color=[:black 2 1], tstop=maximum(get_tstops(p_single)), inputs=[PulseInput(10. .^γ, 1.) for γ = -3:3:3], style=[:solid :solid :solid]);
predictionplot!(
    subplot=2,
    cost_matrix_single,
    mask_cms,
    scales,
    idx_highlight=sample_idx_single,
    opt_targets = [1],
    ylabel = "Fixed-rate cost",
);
predictionplot!(
    subplot=3,
    cost_matrix_single_2,
    mask_cms2,
    scales,
    idx_highlight=sample_idx_single,
    opt_targets = [1],
    ylabel = "Two-step cost",
    color = 2,
);
plot!(
    subplot=4,
    p_single; 
    lw = [1 2 2],
    # legend=:bottomright,
    legend=(1., 0.6),
    plotmodels=[:input, :data, :cascade_2, :fixed_rate_cascade],
    inputs=[noise],
    scale_input=scale_noise,
    tstop=500.,
    color=[:grey :black 2 1],
    label=["Input" "Data" "Two-step" "Fixed-rate"],
);
#============================  Multi-opt figures  =============================# 
scale_noise = 1e-7
plot!(subplot=5, p; lw=[5 2 2], plotmodels=[:data, :cascade_2, :fixed_rate_cascade], color=[:black 2 1], tstop=maximum(get_tstops(p)) * 2/3, inputs=[PulseInput(10. .^γ, 1.) for γ = -3:3:3]);
predictionplot!(
    subplot=6,
    cost_matrix,
    mask_cm,
    scales,
    idx_highlight=sample_idx,
    opt_targets = [1e-3, 1, 1e3],
    ylabel = "Fixed-rate cost",
);
predictionplot!(
    subplot=7,
    cost_matrix_cascade,
    mask_cm2,
    scales,
    color = 2,
    idx_highlight=sample_idx,
    opt_targets = [1e-3, 1, 1e3],
    ylabel = "Two-step cost",
);
plot!(
    subplot=8,
    p; 
    lw = [1 2 2],
    # legend=:bottomright,
    legend=(1., 0.6),
    plotmodels=[:input, :data, :cascade_2, :fixed_rate_cascade],
    inputs=[noise],
    scale_input=scale_noise,
    tstop=500.,
    color=[:grey :black 2 1],
    label=["Input" "Data" "Two-step" "Fixed-rate"],
);
plot!(legend=false);
plot!()

foreach( fmt -> savefig("results/cascade_predictions.$fmt"), [:png, :pdf])

# plot(p_single, plotmodels=[:data], inputs = [PulseInput(10. .^γ, 1.) for γ = -3:3:3]);

