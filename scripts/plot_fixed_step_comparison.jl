using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

using LinearNodes
using Plots
using FileIO
using JLD2


pc = load("parameters/ResultGeneralHomogeneous.jld2", "pc")

pgfplotsx()


cost_fields = [
    :one_node_cost,
    :two_node_cost,
    :three_node_cost,
    :four_node_cost,
    :five_node_cost,
]
param_fields = [
    :one_node_param,
    :two_node_param,
    :three_node_param,
    :four_node_param,
    :five_node_param,
]
# pgfplots()
# gr()
# nr_nodes = 50
cost_scheme = :integral
nr_plots = 5
l = @layout [grid(1, nr_plots); a{0.5h}; grid(1, nr_plots)]
plt = plot(layout = l, size = (600,400));


#==============================================================================#
#========================   Plot the cost scatterplot  ========================# 
#==============================================================================#
for (i, costfield) in enumerate(cost_fields)
    scatter!(
        subplot = nr_plots + 1,
        length.(pc) .- 1,
        getfield.(pc, costfield),
        alpha = 0.5,
        xlims = (0., 25.5),
        ylims = (0., cost_scheme == :integral ? 1.1 : Inf),
        label = "$i-step model"
    );
end
                           
plot!(
    subplot = nr_plots+1,
    legend = :bottomright,
    xlabel = "Pathway length, \$n_{data}\$",
    ylabel = "Cost value",
);
    
#==============================================================================#
#========  Plot margin plots and highlight correspondence in scatter  =========# 
#==============================================================================#
i = 1
for param_field in param_fields
#     p = @eval @filter(pc, length(param) == length($param_field) + 2)[1]
    p = @eval @filter(pc, length(param ) == 7 )[1]
    color = i
    plot!(
        p,
        subplot = i,
        tstop = p.tstop/2,
        plotmodels = [:data, Symbol(split("$param_field", '_')[1])],
        markeralpha = 0.7,
        legend = false,
        color = [:black color],
        ms = 2,
        grid = false,
        ylims = (0, 0.19),
    );
    costfield = Symbol("$param_field"[1:end-5]*"cost")
    scatter!(
        subplot = nr_plots+1,
        [length(p)-1],
        [getfield(p, costfield)],
        color = color,
        ms = 6,
        label = "",
        annotate = [(length(p)-1-0.4, getfield(p, costfield), text("$(('a':'z')[i]))", 11))],
    );
    plot!(subplot = i, ylabel = "");
    global i += 1
end

plot!(subplot = 1, ylabel = "Concentration");
i += 1

for (j, param_field) in enumerate(param_fields)
    p = @eval @filter(pc, length(param ) == length($param_field) + 10)[1]
#     p = [i for i in pc if getfield(param_homogeneous, param_field) == length(i) - 10][1]
    costfield = Symbol("$param_field"[1:end-5]*"cost")
    color = j
    plot!(
        p,
        subplot = i,
        tstop = p.tstop/2,
        plotmodels = [:data, Symbol(split("$param_field", '_')[1])],
        markeralpha = 0.7,
        legend = false,
        color = [:black color],
        ms = 2,
        grid = false,
        ticks = 4,
        ylims = (0, 0.15),
    )
    scatter!(
        subplot = nr_plots+1,
        [length(p)-1],
        [getfield(p, costfield)],
        color = color,
        ms = 6,
        label = "",
        annotate = [(length(p)-1-0.4, getfield(p, costfield) + 0.00, text("$(('a':'z')[i]))", 11))],
    )
    plot!(subplot = i, ylabel = "")
    global i += 1
end

plot!(subplot = nr_plots+2, ylabel = "Concentration");
                            
#==============================================================================#
#==============================  Set the titles  ==============================# 
#==============================================================================#
for i in 1:nr_plots*2+1 
    plot!(plt, subplot = i, title = ('a':'z')[i] * ")", titleloc = :topleft);
end
                                
#==============================================================================#
#===============================  Save to file  ===============================# 
#==============================================================================#
for fmt in [:png, :pdf, :svg]
    savefig(joinpath(
        root,
        "results",
        "homogeneous_cost_multi_nodes_$(cost_scheme).$fmt",
    ))
end
           
