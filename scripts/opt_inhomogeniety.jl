using LinearNodes
using FileIO
using JLD2

spread_param_file = "../parameters/ResultGeneralSpread.jld2"
spread_param = ParamCollection([])

center = 0.

### The default limitation on the parameter search space was too harsh for the 1-step model at spread=3.
### Overwrite that default.
@inline LinearNodes.transform_param(::FixedStepModel, p) = @. 10. ^ (6. * p - 4.)
@inline LinearNodes.transform_param(::FixedRateModel, p) = [10. .^ (2. * p[1] - 1), ceil(30*p[2]), 10. ^ (6. * p[3] - 4.)]

## By design, we have separated the timescales of the different nodes.
## This makes the problem stiff and it takes longer than usual to run.
## Expect this to take a few hours.
for spread in range(0., stop = 3, length = 22)
    println("spread: $spread")
    p = 10. .^ range(center - spread, stop = center + spread, length=11)
    p = [[1.]; p]
    push!(spread_param, opt(DecayingInput(), p))
end

save(spread_param_file, "pc", spread_param)
