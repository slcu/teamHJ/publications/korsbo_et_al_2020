
using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

using LinearNodes
using Plots
using FileIO
using JLD2
using Latexify
path = split(@__DIR__, "scripts")[1]

#= pc = load("$path/parameters/Result.jld2", "pc") =#
pc = load("$path/parameters/ResultGeneral.jld2", "pc")
#= param_set = load("$path/parameters/ResultGamma.jld2", "pc") =#
param_set = @filter(pc, input isa ImpulseInput)

# model_param = :gamma_param
model_param = :fixed_rate_param
model_cost = :fixed_rate_cost

pgfplotsx()

l = @layout [a b c d]
plt = plot(layout=grid(1,4));


######################################################
#  n_model vs n_data
######################################################
n_subplot = 2
scatter!(
    subplot=n_subplot, 
    [length(p.param) for p in param_set], 
    [getfield(p, model_param)[2] for p in param_set],
    alpha=0.3,
    color = 1,
    label="",
);
plot!(subplot=n_subplot, x->x, 0:50,  label="", color=:grey);
plot!(subplot=n_subplot,  ylabel=latexify("n"), ylims=(0., Inf));
plot!(subplot=n_subplot, xlabel="\$n_{data}\$");

######################################################
#  r vs n_data
######################################################

r_subplot = 3
scatter!( subplot=r_subplot,
    [minimum(p.param[2:end]) for p in param_set],
    [getfield(p, model_param)[3] for p in param_set],
    alpha=0.3,
    scale=:log10,
    color = 1,
    label="",
    xlims=(1e-2, 1e1),
    ylims=(1e-2, 1e1),
);
plot!(subplot = r_subplot, x->x, 10. .^(-2:0.1:1), label="", color=:grey);
using LaTeXStrings
plot!(subplot = r_subplot, ylabel=latexify("r"), xlabel=L"min(r_{data})", xrotation=3.14);

######################################################
#  γ vs n_data
######################################################
gamma_subplot=  1
scatter!( subplot=gamma_subplot,
    length.(param_set[:param]),
    [p[1] for p in param_set],
    alpha=0.3,
    color = 1,
    label="",
);
plot!(subplot = gamma_subplot, ylabel=L"\gamma", ylims=(0.,2.), xlabel="\$n_{data}\$");


############################################################
#  cost comparison, Gamma model vs Mass action 
############################################################

cost_subplot = 4
pc = param_set
scatter!(
    subplot= cost_subplot, 
    pc[:two_node_cost],
    pc[model_cost],
#         marker_z = length.(pc[:param]) .- 1,
#         color=:Blues,
#         clims=[1, 25]
#         zcolor = [colormap("Blues",25)[i-1] for i in length.(pc[:param])]',
    alpha = 0.3,
);
percentage_above = @eval round(Int, length(@filter(pc, $(model_cost) > two_node_cost))/length(pc)*100)
plot!(subplot=cost_subplot, annotate=[(0.3, 0.66, "$(percentage_above)\\%")]);
plot!(subplot=cost_subplot, annotate=[(0.7, 0.33, "$(100-percentage_above)\\%")]);
plot!(subplot=cost_subplot, 0:0.1:1, x->x, xlims=(0., 1.), ylims=(0., 1.), color=:grey);
plot!(subplot=cost_subplot, ylabel="Fixed rate cost", xlabel="Two-step cost");
plot!(subplot=cost_subplot, legend=false);


############################################################
#  Plot formatting, saving, and displaying
############################################################

for (i, title) in enumerate(["l)", "m)", "n)", "o)"])
    plot!(subplot = i, title=title, titleloc=:left);
end
plot!(size=(600,130), grid=false);


for format in [:png, :svg, :pdf]
    savefig("$path/results/fixed_rate_parameter_analysis.$format")
end

