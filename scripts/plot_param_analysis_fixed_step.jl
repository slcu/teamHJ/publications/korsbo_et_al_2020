using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

using LinearNodes
using Plots
using FileIO
using JLD2
using Statistics
using Latexify
using LaTeXStrings
using Random
pgfplotsx()

## Load the parameter set
pc = load(joinpath(root, "parameters", "ResultGeneral.jld2"), "pc")
pc = @filter(pc, input isa ImpulseInput)
data(x::ResultGeneral) = x.param
model_parameters(x::ResultGeneral) = x.two_node_param


# plot(layout = (1,2), grid=false, size = (600, 150));
plot(layout = (1,2), grid=false, size = (477.7, 130));
plot!(guidefontsize=10, tickfontsize=7, legendfontsize=10);
#=============================  Fast extra rates  =============================#
subplot = 2
pc_filtered = shuffle(@filter(pc, length(param) == 3))
x = 1:50
y = hcat([sort(p.three_node_param[2:end]) for p in pc_filtered][x]...)
y_data = hcat([sort(p.param[2:end]) for p in pc_filtered][x]...)
scatter!(
    subplot = subplot,
    x,
    y',
    yscale = :log10,
    label = [L"r_{slow}" L"r_{fast}" L"r_{extra}"],
);
scatter!(
    subplot = subplot,
    x,
    y_data',
    yscale = :log10,
    markershape = :cross,
    # label = ["True values" ""],
    label = "",
    ms = 3,
);
plot!(subplot = subplot, legend = :topright, xlabel = "Parameter set", ylabel = "Parameter value");
#==========================  Order does not matter  ===========================#
p = @filter(pc, length(param) == 10)[1]
tstop = 800
order_subplot = 1
plot!(
    subplot = order_subplot,
    simulate(get_model(p), p.input, p, tstop = tstop),
    vars = 2,
    lw = 5,
    label = "Original",
);
plot!(
    subplot = order_subplot,
    simulate(get_model(p), p.input, [p[1]; sort(p[2:end])], tstop = tstop),
    vars = 2,
    style = :dash,
    lw = 5,
    label = "Sorted",
);
plot!(
    subplot = order_subplot,
    xlims = (0.0, tstop),
    ylims = (0.0, 0.005),
    yticks = 0:0.002:0.01,
    xticks = 0:200:1000,
    ylabel = "Concentration",
    xlabel = "Time",
    legend = :topright,
    grid = false,
);
foreach(
    i -> plot!(subplot = i; title = "$(('A':'Z')[i])"),
    eachindex(plot!().subplots),
)


plot!(dpi=600);
plotname = "fixed_step_rate_order"
foreach(fmt -> savefig("results/$plotname.$fmt"), [:pdf, :png])
plot!(dpi=90);

