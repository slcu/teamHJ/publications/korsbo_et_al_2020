using LinearNodes
using FileIO
using JLD2

param_file_homogeneous = joinpath("..", "parameters", "ResultGeneralHomogeneous.jld2")

pc_homo = ParamCollection([])

## Override the default search space of n ∈ {1..30}
@inline LinearNodes.transform_param(::FixedRateModel, p) = [10. .^ (2. * p[1] - 1), ceil(60*p[2]), 10. ^ (4. * p[3] - 2.)]
@inline LinearNodes.transform_param(::LinearNodes.AbstractFunctionalModel, p) = [10. ^ (6*p[1] - 4), 60. * p[2] + 1, 10. ^ (6*p[3] - 3)]
## The paper only contains analyses of the fixed-step models. 
## These manual overrides are not documented since they are not used.

for nodes in 1:50
    println("opt $nodes")
    p = opt_homogeneous(DecayingInput(), nr_data_nodes=nodes)
    push!(pc_homo, p)
end

save(param_file_homogeneous, "pc", pc_homo)

