using Distributed
using LaTeXStrings
using LinearNodes
using Plots
using FileIO
using PGFPlotsX
using Colors
using Pkg

root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

overwrite = false
if isfile("parameters/n_data_vs_inhomogeniety.jld2") && !overwrite
    pc_hetro = load("parameters/n_data_vs_inhomogeniety.jld2")
    pc_hetro_f = pc_hetro["fixed_rate"]
    pc_hetro_2 = pc_hetro["two_step"]

    δrange = getfield.(pc_hetro_f[1,:], :δ)
    nrange = getfield.(pc_hetro_f[:,1], :n)

    @assert getfield.(pc_hetro_2[1,:], :δ) == δrange
    @assert getfield.(pc_hetro_2[:,1], :n) == nrange
else 
    nprocs() == 1 && addprocs(8)
    @everywhere using Pkg
    @everywher Pkg.activate(root)
    @everywhere using LinearNodes
    #
    nrange = 1:25
    δrange = 0:0.1:3
    M = [(n, δ) for n in nrange, δ in δrange]
    #
    ## Force compilation
    @everywhere LinearNodes.opt_heterogeneous(FixedRateModel(); n=1, δ=0.)
    @everywhere LinearNodes.opt_heterogeneous(FixedStepModel(); n=2, δ=0.)
    #
    ## This one is pretty quick
    @time pc_hetro_f = pmap(x -> LinearNodes.opt_heterogeneous(FixedRateModel(); n=x[1], δ=x[2]), M)
    # @time pc_hetro_f = map(x -> LinearNodes.opt_heterogeneous(FixedRateModel(); n=x[1], δ=x[2]), M)
    #
    ## This one takes about 4 minutes using 8 threads
    @time pc_hetro_2 = pmap(x -> LinearNodes.opt_heterogeneous(FixedStepModel(); n=x[1], δ=x[2]), M)
    #
    save("parameters/n_data_vs_inhomogeniety.jld2", "fixed_rate", pc_hetro_f, "two_step", pc_hetro_2)
end



plotting_package = :pgfplotsx
if plotting_package == :pgfplotsx

    @pgf plt1 = Plot3(
        {
            surf,
            shader = "flat corner",
            "point meta min" = -2,
            "point meta max" = 0,
        },
        Coordinates(nrange, δrange, log10.(getfield.(pc_hetro_2, :cost)))
    );
#
    @pgf plt2 = Plot3(
        {
            surf,
            shader = "flat corner",
            "point meta min" = -2,
            "point meta max" = 0,
        },
        Coordinates(nrange, δrange, log10.(getfield.(pc_hetro_f, :cost)))
    );
#
    @pgf plt3 = Plot3(
        {
            surf,
            shader = "flat corner",
            "point meta min" = -2,
            "point meta max" = 2,
        },
        Coordinates(nrange, δrange, log10.(getfield.(pc_hetro_f, :cost) ./ getfield.(pc_hetro_2, :cost)))
    );
#
    gp = @pgf GroupPlot(
        {
            group_style = {
                group_size="3 by 1",
                horizontal_sep="70pt",
        },
        view = (0, 90),
        colorbar,
        colorbar_style = {
            ytick="-2, -1, 0, 1, 2",
        },
        ytick="0, 1, 2, 3",
        width = "115pt",
        ylabel = L"\delta",
        xlabel = L"n_{data}",
        tick_align = "center",
        major_tick_length = "0.08cm",
        });
#
    @pgf push!(gp,
        {
            title="G",
            "colormap/viridis",
            colorbar_style = { title=L"\log_{10}(Cost_{\textrm{\tiny two-step}})", },
        },
        plt1,
    );
#
    @pgf push!(gp,
        {
            title="H",
            "colormap/viridis",
            colorbar_style = { title=L"\log_{10}(Cost_{\textrm{\tiny fixed-rate}})", },
        },
    plt2
    );
#
    @pgf push!(gp, 
        {
            title="I",
            colormap_name = "RdBu",
            colorbar_style = { title=L"\log_{10}(\frac{Cost_{\textrm{\tiny fixed-rate}}}{Cost_{\textrm{\tiny two-step}}})", },
        },
        plt3,
    );
#
    td = TikzDocument();
    push_preamble!(td, ("RdBu", Colors.colormap("RdBu")));
    tp = TikzPicture();
    push!(td, tp);
    push!(tp, gp);
    td


    figname = "n_data_vs_inhomogeneity_pgfplotsx"
    pgfsave("results/$figname.png", td; dpi = 600)
    pgfsave("results/$figname.pdf", td; dpi = 600)
    pgfsave("results/$figname.svg", td; dpi = 600)

elseif plotting_package == :Plots
    gr()
pgfplotsx()

    l = @layout grid(1,3)
    plot(layout=l, size=(600,200), grid=false);

    heatmap!(
        subplot=1,
        nrange,
        δrange,
        log10.(getfield.(pc_hetro_2, :cost)) |> permutedims;
        clims=(-2.,0),
        # colorbar_title = "Two-step cost [log10]",
        colorbar_title = L"C_{two-step}",
        ylabel = raw"$\delta$",
        xlabel = raw"$n_{data}$",
        color = :viridis,
    );

    heatmap!(
        subplot=2,
        nrange,
        δrange,
        log10.(getfield.(pc_hetro_f, :cost)) |> permutedims;
        clims=(-3.,0),
        colorbar_title = "Fixed rate cost [log10]",
        ylabel = raw"$\delta$",
        xlabel = raw"$n_{data}$",
        color = :viridis,
    );


    heatmap!(
        subplot=3,
        nrange,
        δrange,
        log10.(getfield.(pc_hetro_f, :cost) ./ getfield.(pc_hetro_2, :cost)) |> permutedims; 
        color=:balance,
        colorbar_title="Cost ratio [log10]",
        ylabel = raw"$\delta$",
        xlabel = raw"$n_{data}$",
        clims=(-2, 2),
    );
    plot!()

    figname = "n_data_vs_inhomogeneity"
    savefig("results/$figname.png")
    savefig("results/$figname.pdf")
    savefig("results/$figname.svg")

end

