using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

using LinearNodes
using FileIO
using Plots
pgfplotsx()
using DifferentialEquations
using Statistics
using ProgressMeter

path = split(@__DIR__, "notebooks")[1]
pc = load("$root/parameters/ResultGeneral.jld2", "pc")
pc_decaying = @filter pc input isa ImpulseInput
pcd25 = @filter pc_decaying length(param) <= 21
pc_linear = pcd25
length(pc_linear)

pcs = load("$root/parameters/ResultMultiCascadeSingleRedefined.jld2", "pc")
length(pcs)


p = pcs[2]
@time int = saturation_integral(p; force_dtmin=true, alg=Rosenbrock23())
@time int = saturation_integral(pc_linear[1];force_dtmin=true,  alg=Rosenbrock23())

sat_linear = @showprogress [saturation_integral(p; force_dtmin=true, alg=Rosenbrock23()) for p in pc_linear]
sat = @showprogress [saturation_integral(p; force_dtmin=true, alg=Rosenbrock23()) for p in pcs]


function geometric_median_index(pc, sat, mask)
    _, median_index = findmin(map(i->sum(sqrt.( (sat[mask] .- sat[i]).^2 .+ (cost.(pc[mask], :cascade_2) .- cost(pc[i], :cascade_2)).^2)), eachindex(sat)[mask]))
    return eachindex(sat)[mask][median_index]
end

#===================================  Plot  ===================================# 
l = @layout [grid(1,3)
             grid(1,4)]
plot(layout=l, legend=false, grid=false, size=(800,300));
subplots = Dict(
                :fixed_rate_cost => 1,
                :two_step_cost => 2,
                :cost_comparison => 3,
                :length => 4,
                :examples => [4, 5, 6, 7],
               )
mask = (0 .< sat .< 1) .*
    (0 .< cost.(pcs, :fixed_rate_cascade) .< 1) .*
    (0 .< cost.(pcs, :cascade_2) .< 1);
scatter!(
         subplot = subplots[:fixed_rate_cost],
    sat[mask],
    cost.(pcs, :fixed_rate_cascade)[mask],
    xlims=(0.7, 1.),
    ylims=(0., 1.),
    alpha = 0.2,
    color = 1,
    ylabel="Fixed-rate cost",
    xlabel = "Saturation score",
);
scatter!(
         subplot = subplots[:two_step_cost],
    sat[mask],
    cost.(pcs, :cascade_2)[mask],
    xlims=(0.7, 1.),
    ylims=(0., 1.),
    alpha = 0.2,
    color = 2,
    ylabel="Two-step cost",
    xlabel = "Saturation score",
);
scatter!(
    subplot=subplots[:cost_comparison],
    cost.(pcs[mask], :cascade_2),
    cost.(pcs[mask], :fixed_rate_cascade),
    xlabel = "Two-step cost",
    ylabel = "Fixed-rate cost",
    alpha = 0.05,
    color = :black,
    scale = :log10,
);
plot!(
    subplot=subplots[:cost_comparison],
    [1e-5, 1],
    x->x,
    ylim = [1e-5, 1],
    xlim = [1e-5, 1],
    label="",
    color=:black,
    alpha = 0.5,
   );
example_idxs = [
                geometric_median_index(pcs, sat, (0.7 .< sat .< 0.8) .* mask),
                eachindex(pcs)[(0.8 .< sat .< 0.83) .* (cost.(pcs, :cascade_2) .> 0.3)][1],
                geometric_median_index(pcs, sat, (0.80 .< sat .< 0.9) .* mask),
                geometric_median_index(pcs, sat, (0.90 .< sat .< 1) .* mask),
               ]
tstops = [40., 80., 100., 300.]
for (i, subplot) in enumerate(subplots[:examples])
    p = pcs[example_idxs[i]]
    plot!(
          subplot=subplot,
          p,
          plotmodels = [:data, :cascade_2, :fixed_rate_cascade],
          color=[:black 2 1],
          alg=Rosenbrock23(autodiff=false),
          label = ["Data" "Two-step" "Fixed-rate"],
          tstop=tstops[i],
        );
    scatter!(
             subplot = subplots[:fixed_rate_cost],
             [sat[example_idxs[i]]],
             [cost(p, :fixed_rate_cascade)],
             ms = 7,
             shape=:diamond,
             color=:white,
             annotate = (
                         [sat[example_idxs[i]]],
                         .25 .+ [cost(p, :fixed_rate_cascade)],
                         [text("$(('A':'Z')[subplot])", 9)],
                        )
            );
    scatter!(
             subplot = subplots[:two_step_cost],
             [sat[example_idxs[i]]],
             [cost(p, :cascade_2)],
             ms = 7,
             shape=:diamond,
             color=:white,
             annotate = (
                         [sat[example_idxs[i]]],
                         .3 .+ [cost(p, :cascade_2)],
                         [text("$(('A':'Z')[subplot])", 9)],
                        )
            );
    scatter!(
             subplot = subplots[:cost_comparison],
             [cost(p, :cascade_2)],
             [cost(p, :fixed_rate_cascade)],
             ms = 7,
             shape=:diamond,
             color=:white,
             annotate = (
                [cost(p, :cascade_2)],
                [[5e-1, 2e-3, 5e-1, 5e-4][i]],
                # 0.1 .* [cost(p, :fixed_rate_cascade)],
                # [1e-4],
                [text("$(('A':'Z')[subplot])", 9)],
               )
            );
end;
plot!(subplot=subplots[:examples][1], legend=:topright);
foreach(i -> plot!(subplot=i, title="$(('A':'Z')[i])"), eachindex(plot!().subplots));
plot!(title_location = (0.5, 0.85));
plot!()

savefig("results/cost_vs_saturation.pdf")
savefig("results/cost_vs_saturation.png")
