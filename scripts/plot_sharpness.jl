using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

using LinearNodes
using Plots
using FileIO
using JLD2
using LaTeXStrings
using Statistics
# using Colors

pgfplotsx()

pc = load("parameters/ResultGeneral.jld2", "pc")
pc = @filter pc input isa ImpulseInput

ms = 4.

pc_filtered = @filter(pc, length(param) == 26)
median_idx = sortperm(t_mean.(pc_filtered) .^ 2 ./ t_var.(pc_filtered))[round(Int, end / 2)]
p = pc_filtered[median_idx]
#
l = @layout(grid(2, 2))
plot(layout = l; grid = false, size = (600, 300));
#=============================  Sharpness plots  ==============================#
sharpnessplot!(
    subplot = 1,
    pc,
    plotmodels = [:data, 5, 2],
    legend = :topleft,
    p_highlight = [p],
    yticks = (  
              vcat(0:6, sqrt(5), sqrt(2)),
              # LaTeXString.(string.([0:6; L"$$\sqrt{5}$$"]))
              LaTeXString.(vcat([i % 3 == 0 ? string(i) : "" for i in  0:6], L"$$\sqrt{5}$$", L"$$\sqrt{2}$$")) 
             ),
    markerstrokealpha = [0.1 0.1 0.1 1 1 1],
);
# sharpnessplot!(
#     subplot = 1,
#     pc,
#     plotmodels = [:data, 2],
#     legend = :topleft,
#     p_highlight = [p],
#     yticks = (  
#               [0:6; sqrt(2)],
#               LaTeXString.([[i % 3 == 0 ? string(i) : "" for i in  0:6]; L"$$\sqrt{2}$$"])
#              ),
#     yminorticks= 2,
# );
#======================  rate homogeneity vs sharpness  =======================#
subplot = 2
scatter!(
    subplot = subplot,
    sharpness.(pc),
    rate_homogeneity.(pc),
    alpha = 0.1,
    markercolor = :black,
    label = "Data",
    markerstrokealpha = 0.1,
    markersize = ms,
);
for i in [5, 2]
    pc_longer = @filter(pc, length(param) - 1 >= i)
    scatter!(
        subplot = subplot,
        sharpness.(pc_longer),
        rate_homogeneity.(pc_longer, i),
        markercolor = i,
        alpha = 0.3,
        label = "$i-step model",
        markerstrokealpha = 0.1,
        markersize = ms,
    )
    scatter!(
        subplot = subplot,
        [sharpness(p)],
        [rate_homogeneity(p, i) rate_homogeneity(p)],
        color = :white,
        label = "",
        shape = :diamond,
        ms = 6,
    )
end
xticks!(
    subplot = subplot,
    [1:4; [sqrt(i) for i in [2, 5]]],
    LaTeXString.(string.([1:4; ["\$\$\\sqrt{$i}\$\$" for i in [2, 5]]])),
);
plot!(
    subplot = subplot,
    xlabel = L"\textrm{Data sharpness, }s_{data}",
    # ylabel = L"\textrm{Rate dispersion, }\sigma_r/\langle r \rangle",
    ylabel = "Rate dispersion",
    legend = :topright,
    ylims = (0., 3.5),
    yticks = 0:3,
);
#======================  rate homogeneity vs n_data  =======================#
subplot = 3
scatter!(
    subplot = subplot,
    length.(pc) .- 1,
    rate_homogeneity.(pc),
    alpha = 0.1,
    markercolor = :black,
    label = "Data",
    markerstrokealpha = 0.1,
    markersize = ms,
);
for i in [5, 2]
    pc_longer = @filter(pc, length(param) - 1 >= i)
    scatter!(
        subplot = subplot,
        length.(pc_longer) .- 1,
        rate_homogeneity.(pc_longer, i),
        markercolor = i,
        alpha = 0.3,
        label = "$i-step",
        markerstrokealpha = 0.1,
        markersize = ms,
    )
    scatter!(
        subplot = subplot,
        [length(p) - 1],
        [rate_homogeneity(p, i) rate_homogeneity(p)],
        color = :white,
        label = "",
        shape = :diamond,
        ms = 6,
    )
end
plot!(
    subplot = subplot,
    xlabel = L"n_{data}",
    # ylabel = L"\textrm{Rate dispersion, }\sigma_r/\langle r \rangle",
    ylabel = "Rate dispersion",
    legend = :topright,
    ylims = (0., 3.5),
    yticks = 0:3,
);
#=============================  Trajectory demo  ==============================#
plot!(
    subplot = 4,
    p; 
    plotmodels = [:data, :five_node, :two_node],
    legend = :topright,
    label=["Data" "Five-step" "Two-step"],
    plotcolors=Dict(:two_node => 2, :five_node=>5),
    ylims = (0., 0.0043),
);
plot!(title = permutedims(["$(('A':'Z')[i])" for i in 1:10]), title_location = (0.5,0.9));
# foreach(
#     i -> plot!(subplot = i; title = "$(('a':'z')[i]))"),
#     eachindex(plot!().subplots),
# )
plot!(subplot=1; legend=false);
plot!(subplot=2; legend=false);
plot!(subplot=3; legend=false);

plot!(dpi=600);
plotname = "fixed_step_sharpness_analysis"
foreach(fmt -> savefig("results/$plotname.$fmt"), ["png", "pdf"])
plot!(dpi=90);
