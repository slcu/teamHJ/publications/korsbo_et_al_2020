@inline param(p::AbstractParameter) = p.param
@inline param(p::Union{Result, ResultMulti}, model::AbstractModel) = p.fitted_models[model][1]
@inline param(p::ResultGeneral, model::AbstractModel) = p.fitted_models[model][1]
@inline param(pc::AbstractParameterCollection) = hcat(getfield.(pc, :param)...)

function param(p::Union{Result, ResultMulti}, model::Symbol) 
    model = split(string(model), "_param")[1]
    model = Symbol(model)
    p.fitted_models[model][1]
end

get_tstops(p::AbstractParameter) = [p.tstop]
get_tstops(p::ResultMulti) = p.tstops

hasmodel(p::Union{Result, ResultMulti}, s::Symbol) = haskey(p.fitted_models, s)
hasmodel(p::ResultGeneral, s::Symbol) = hasfield(typeof(p), Symbol("$(s)_param"))

function param(p::ResultGeneral, model::Symbol) 
    model = split(string(model), "_param")[1]
    model = Symbol(model * "_param")
    getfield(p, model)
end

@inline cost(p::ResultGeneral) = p.cost
@inline cost(p::Union{Result, ResultMulti}, model::Symbol) = p.fitted_models[model][2]
@inline cost(p::ResultGeneral, model::Symbol) = getfield(p, Symbol("$(model)_cost"))
@inline cost(pc::AbstractParameterCollection) = getfield.(pc, :cost)

@inline get_model(p::AbstractParameter) = p.model

data_model(p::AbstractParameter) = p.model
get_inputs(p::ResultGeneral) = [p.input]
get_inputs(p::Union{Result, ResultMulti}) = p.inputs

function get_model(s)
    fixed_step_names = [1, :one_step, :one_node, :one,
                        2, :two_step, :two_node, :two,
                        3, :three_step, :three_node, :three,
                        4, :four_step, :four_node, :four,
                        5, :five_step, :five_node, :five,
                       ]
    s in fixed_step_names && return FixedStepModel()
    s in [:fixed_rate] && return FixedRateModel()
    s in [:dde] && return DDEModel()
    s in [:gamma] && return GammaGeneralModel()
    s in [:fixed_rate_mm] && return FixedRateMMModel()
    split(string(s), "_")[1] == "mm" && return MMChainModel()
    s in [:fixed_rate_hill] && return FixedRateHillModel()
    s in [:cascade] && return CascadeModel()
    s in [:cascade] && return CascadeModel()
    split(string(s), "_")[1] == "cascade" && return CascadeModel()
    s in [:fixed_rate_cascade] && return FixedRateCascadeModel()
    error("non-existent key to get_model()")
end

function name(s::Symbol)
    s == :fixed_rate && return "Fixed-rate"
    s == :two_node && return "Two-step"
    s == :three_node && return "Three-step"
    s == :four_node && return "Four-step"
    s == :five_node && return "Five-step"
    s == :dde && return "DDE"
    s == :gamma && return "Gamma"
    s == :cascade_1 && return "One-step"
    s == :cascade_2 && return "Two-step"
    s == :fixed_rate_cascade && return "Fixed-rate"
    s == :data && return "Data"
    s == :input && return "Input"
end

name(::FixedRateModel) = :fixed_rate
name(::FixedStepModel) = :fixed_step

fitted_models(p::ResultGeneral) = [:one_node, :two_node, :three_node, :four_node, :five_node, :fixed_rate, :gamma, :dde]
fitted_models(p::Union{Result, ResultMulti}) = collect(keys(p.fitted_models))

@inline function node_param(::AbstractParameter, nr_nodes)
    nr_nodes == 1 && return :one_node_param
    nr_nodes == 2 && return :two_node_param
    nr_nodes == 3 && return :three_node_param
    nr_nodes == 4 && return :four_node_param
    nr_nodes == 5 && return :five_node_param
end

@inline function node_cost(::AbstractParameter, nr_nodes)
    nr_nodes == 1 && return :one_node_cost
    nr_nodes == 2 && return :two_node_cost
    nr_nodes == 3 && return :three_node_cost
    nr_nodes == 4 && return :four_node_cost
    nr_nodes == 5 && return :five_node_cost
end


function standardise_fixed_step_name(i)
    if i in [1:5; [:one, :two, :three, :four, :five]; [:one_node, :two_node, :three_node, :four_node, :five_node]]
        if i isa Symbol
            if i in [:one, :two, :three, :four, :five]
                nodes = Symbol("$(i)_node")
                # i = findfirst(x -> x == nodes, [:one, :two, :three, :four, :five])
                return nodes
            elseif i in [:one_node, :two_node, :three_node, :four_node, :five_node]
                nodes = i
                # i = findfirst(x -> x == nodes, [:one_node, :two_node, :three_node, :four_node, :five_node])
                return nodes
            else
                error("Unreachable reached.")
            end
        else
            nodes = [:one_step, :two_step, :three_step, :four_step, :five_step][i]
            return nodes
        end
    else 
        return i
    end
end


@inline function maparam(p::AbstractParameter, nr_nodes)
    getfield(p, node_param(p, nr_nodes))
end
@inline function macost(p::AbstractParameter, nr_nodes)
    getfield(p, node_cost(p, nr_nodes))
end

function geometric_median(pc::AbstractParameterCollection, field1, field2)
    x, y = pc[field1], pc[field2]
    _, median_index = findmin(map(i->sum(sqrt.( (x .- x[i]).^2 .+ (y .- y[i]).^2)), eachindex(x)))
    return pc[median_index]
end

function increment_node(s::Symbol)
    s == :one_node && return :two_node
    s == :two_node && return :three_node
    s == :three_node && return :four_node
    s == :four_node && return :five_node
    @error "Invalid input to `increment_node`"
end

function reset_yticks!(plt, subplot; k_min=3, k_max=5, k_ideal=3, strict_span=true)
    ylims = Plots.axis_limits(plt.subplots[subplot], "y")
    ylims = (ylims[1], ylims[2] * 1.05) ## don't cut thick lines
    yticks, lim_min, lim_max = Plots.optimize_ticks(ylims...; k_min=3, k_max=5, k_ideal=3, strict_span=true)
    plot!(plt, subplot=subplot, yticks=yticks, ylims=(lim_min, lim_max))
end
reset_yticks!(plt; kwargs...) = map(subplot -> reset_yticks!(plt, subplot; kwargs...), 1:length(plt.subplots))


t_mean(p::AbstractParameter, ::FixedStepModel, nodes=2) = sum(1 ./ getfield(p, node_param(p, nodes))[2:end])
t_mean(p::AbstractParameter, nodes::Int) = t_mean(p, FixedStepModel(), nodes)
t_mean(p::AbstractParameter) = sum(1 ./ p[2:end])

t_var(p::AbstractParameter, ::FixedStepModel, nodes=2) = sum(1 ./ getfield(p, node_param(p, nodes))[2:end] .^ 2)
t_var(p::AbstractParameter, nodes::Int) = t_var(p, FixedStepModel(), nodes)
t_var(p::AbstractParameter) = sum(1 ./ p[2:end] .^ 2)

t_std(args...) = sqrt(t_var(args...))

sharpness(args...) = t_mean(args...) / t_std(args...)

r_mean(p::AbstractParameter) = mean(p[2:end])
r_mean(p::AbstractParameter, nodes::Int) = mean(getfield(p, node_param(p, nodes))[2:end])

r_std(p::AbstractParameter) = std(p[2:end])
r_std(p::AbstractParameter, nodes::Int) = std(getfield(p, node_param(p, nodes))[2:end])

rate_homogeneity(args...) = r_std(args...) / r_mean(args...)
